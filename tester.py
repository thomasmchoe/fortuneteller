#!/usr/bin/python
#-*- coding : utf-8 -*-

import requests
import json
from pprint import pprint
import dateutil.parser
import pytz
import math
import numpy
import matplotlib.pyplot as plt
# import selectUser

# esID = 'e1wljthualvjgu1thdag'
esID = '41-n-bdqggn1f2bzd5gl'
startDate = '2016-01-01'
endDate = '2016-02-01'

baseURL = "http://walkdev.zikto.com:8080/getTotalTrend?"
argument = "userId=" + esID + "&startDate="+ startDate + "&endDate="+endDate
url = baseURL + argument

r = requests.get(url)
d = json.loads(r.content)

print d

local_tz = pytz.timezone(timeZone)

startDateTime = dateutil.parser.parse(startDate)
endDateTime = dateutil.parser.parse(endDate)

if u'intensives' in d:
    for walk in d[u'intensives']:
        weekData = numpy.zeros((1,24))
        startTime = dateutil.parser.parse(walk[u'start'])
        endTime = dateutil.parser.parse(walk[u'end'])

        startDateLocal = startTime.replace(tzinfo=pytz.utc).astimezone(local_tz)
        endDateLocal = endTime.replace(tzinfo=pytz.utc).astimezone(local_tz)

        key = startDateLocal.strftime('%Y%m%d_'+esID)

        if key in data:
            res = data[key]
            res[0, startDateLocal.hour] = res[0,startDateLocal.hour] + walk[u'totalPedo']
        else:
            res = numpy.zeros((1,24))
            res[0, startDateLocal.hour] = walk[u'totalPedo']

        data[key]=res
